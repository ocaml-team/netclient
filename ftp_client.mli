(* $Id: ftp_client.mli 146 2005-07-28 14:00:45Z gerd $ *)

(* TODO:
 * 
 * - Timeouts
 * - Reasonable exception handling. Currently, exceptions simply fall through
 *   to the caller which is always Unixqueue.run
 * - ftp_connected, ftp_data_conn are probably not correctly set
 *)

(** FTP client
  *
  * Warning: Experimental code! Not all features described in this interface
  * work as described!
  *
  * The client is currently only partially implemented. Especially, the
  * STOR and STOU commands are missing. Only IPv4 is supported.
  *
  * It is intended to implement a large subset of RFC 959 (and later,
  * the newer FTP-related RFCs). This includes support for EBCDIC,
  * record files, and block mode.
 *)

exception FTP_protocol_error of exn
  (** Something went wrong, often on socket level *)

exception FTP_protocol_violation of string
  (** The server violates the FTP specification *)


type cmd_state =
    [ `Init
    | `Success
    | `Proto_error
    | `Failure
    | `Rename_seq
    | `Restart_seq
    | `User_pass_seq
    | `User_acct_seq
    | `Pass_acct_seq
    | `Preliminary
    ]
  (** The command state:
    * - [`Init]: Just connected, no greeting message arrived yet
    * - [`Success]: Got the greeting message/last command was successful
    * - [`Proto_error]: {b currently unused}
    * - [`Failure]: Last command was not successful
    * - [`Rename_seq]: Used instead of [`Success] after the RNFR command
    * - [`Restart_seq]: Used instead of [`Success] after the REST command
    * - [`User_pass_seq]: Used instead of [`Success] after the USER command
    *   when a password must be typed in
    * - [`User_acct_seq]: Used instead of [`Success] after the USER command
    *   when an account ID must be typed in
    * - [`Pass_acct_seq]: Used instead of [`Success] after the PASS command
    *   when an account iD must be typed in
    * - [`Preliminary]: a reply with code 100 to 199. There will be another
    *   final reply for the command
   *)

type port =
    [ `Active of string * int * Unix.file_descr
    | `Passive of string * int
    | `Unspecified 
    ]
  (** The port of the data connection: [`Active] means that the server 
    * initiates the data connection to the listening client, and in the
    * case of [`Passive] the client initiates the data connection to the
    * listening server. The string argument is the IP address as dotted
    * quad, the int argument is the port number, and the descriptor 
    * is the listening master socket.
   *)


type form_code =
    [ `Non_print | `Telnet | `ASA ]
  (** The [form_code] has a meaning when FTP is used to print files:
    * - [`Non_print]: This is not the case.
    * - [`Telnet]: Telnet control codes are used for vertical movement
    * - [`ASA]: ASA (Fortran) control codes are used for vertical movement
   *)

type representation =
    [ `ASCII of form_code option
    | `EBCDIC of form_code option
    | `Image
    ]
  (** The representation of the transferred file:
    * - [`ASCII]: An ASCII variant is used, i.e. the server sends files in
    *   ASCII encoding with CR/LF as end-of-line marker. Supported by all
    *   servers.
    * - [`EBCDIC]: An EBCDIC variant is used, i.e. the server sends files in
    *   EBCDIC encoding with NEL as end-of-line marker
    * - [`Image]: The file is transferred in its original binary
    *   representation. Supported by all servers.
    *
    * "Local" representations are not supported.
    *
    * This FTP client does not recode the files such that they match the
    * selected representation. When files are downloaded, they are stored
    * as they are received. When files are uploaded, they are sent as they
    * are. The user of this client must do recodings when necessary
    * (the class {!Ftp_data_endpoint.data_converter} may be useful for this).
    *
    * If no representation is selected, FTP servers assume [`ASCII None]
    * as default.
   *)


type structure =
    [ `File_structure
    | `Record_structure
    ]
  (** The client supports two structures:
   * - [`File_structure]: Files are simply contiguous streams of bytes
   * - [`Record_structure]: Files are sequences of records. FTP does
   *   not make a difference between variable and fixed length records.
   *   It is not forbidden that the records are themselves structured
   *   into lines, in fact it can happen that end-of-line markers are
   *   contained in binary records. Operating systems that support 
   *   record-structured files often store text files in this format, i.e.
   *   every line is stored in its own record, without end-of-line marker.
   *   If record structure is selected by a STRU command, it is recommended
   *   to use the classes {!Ftp_data_endpoint.out_record_channel} and
   *   {!Ftp_data_endpoint.in_record_channel} for the local representation
   *   of the files, otherwise the records may be incorrectly mapped
   *   to the local conventions.
   *
   * Page-structured files (i.e. indexed files) are not supported.
   *
   * If no structure is selected, FTP servers will assume file structure
   * as default.
   *)


type transmission_mode =
    [ `Stream_mode
    | `Block_mode
    ]
  (** The transmission mode selects how the data are encoded in the data
    * connection.
    * - [`Stream_mode]: This is the simple format that is responsible for
    *   all the failed FTP downloads. It is supported by all FTP servers,
    *   actually, you cannot assume a better transmission mode from an
    *   unknown FTP server. It is unreliable because it cannot distinguish
    *   between a transmission failure and the regular end-of-file condition.
    * - [`Block_mode]: This is an improved format using frames to protect
    *   the transmitted data. Unfortunately, almost no FTP server supports
    *   it.
    *
    * Both modes are compatible with both structures, i.e. you can transfer
    * a record-structured file in stream mode and a flat file in block
    * mode. However, in practise this is not the case. Servers that only
    * know flat files are likely to only support stream mode, and servers
    * implementing record structure imply that block transfers base on
    * the record format. So the advise is to use stream mode for flat
    * files, and block mode for record files.
   *)


type state =
    { cmd_state : cmd_state;        (** the command state *)
      ftp_connected : bool;         (** whether connected with the server *)
      ftp_data_conn : bool;         (** whether there is a clean data conn *)
      ftp_user : string option;     (** successfully sent user identifier *)
      ftp_password : string option; (** successfully sent password *)
      ftp_account : string option;  (** successfully sent account identifier *)
      ftp_logged_in : bool;         (** whether the user is logged in *)
      ftp_port : port;              (** the selected port *)
      ftp_repr : representation;    (** the selected representation *)
      ftp_structure : structure;    (** the selected structure *)
      ftp_trans : transmission_mode; (** the selected trans mode *)
      ftp_dir : string list;
         (** The current directory, expressed as list of CWD changes minus
          * CDUP changes. This is only reasonable if CWD does not include
          * slashes. The list is in reverse order, i.e. deepest directory
          * first.
          *)
    }
  (** The state reflects the knowledge of the client about what has been
    * agreed upon with the server.
   *)


type cmd =
    [ `Connect
    | `USER of string
    | `PASS of string
    | `ACCT of string
    | `CWD of string
    | `CDUP
    | `SMNT of string
    | `QUIT
    | `REIN
    | `PORT   (* port is automatically chosen *)
    | `PASV
    | `TYPE of representation
    | `STRU of structure
    | `MODE of transmission_mode
    | `RETR of string * (state -> Ftp_data_endpoint.local_receiver)
    | `STOR of string * (state -> Ftp_data_endpoint.local_sender)
    | `STOU of (unit -> Ftp_data_endpoint.local_sender)
    | `APPE of string * (state -> Ftp_data_endpoint.local_sender)
    | `ALLO of int * int option
    | `REST of string
    | `RNFR of string
    | `RNTO of string
    | `DELE of string
    | `RMD of string
    | `MKD of string
    | `PWD
    | `LIST of string option * (state -> Ftp_data_endpoint.local_receiver)
    | `NLST of string option * (state -> Ftp_data_endpoint.local_receiver)
    | `SITE of string
    | `SYST
    | `STAT of string option
    | `HELP of string option
    | `NOOP
    ]
  (** An FTP command. {b Currently, STOR, STOU and APPE are not supported.} *)

type reply = int * string
  (** Reply code plus text *)



(** The client protocol interpreter...
 *
 * has a queue of commands that are sent to the server in turn.
 *)
class ftp_client_pi : 
        ?event_system:Unixqueue.event_system ->
        ?onempty:(state -> unit) ->
        ?onclose:(unit -> unit) ->  (* TODO, currently ignored *)
        Unix.file_descr ->
object


  method add_cmd : ?onreply:(state -> reply -> unit) -> cmd -> unit
    (** Add another command to the queue. The protocol interpreter does 
     * not check whether this command is allowed in the current state
     * or not. For every reply of the server [onreply] is called.
     * Due to the FTP specification there may be several replies for
     * a command: First, zero or more replies with [cmd_state = `Preliminary],
     * and then exactly one reply with a final state.
     *)

  method send_abort : unit -> unit
    (** Sends immediately an [ABOR] command, even when a data transfer is
     * in progress.
     *
     * TODO - not yet implemented
     *)

  method run : unit -> unit
    (** Starts the event system; same as [Unixqueue.run] *)

  method state : state
    (** Returns the current state *)

end


(** An [ftp_method] is a small procedure doing some task *)
class type ftp_method =
object
  method connect : (string * int) option
    (** The host and port the FTP method wants to be connected to.
      * If [None] the current connection is used.
     *)

  method execute : ftp_client_pi -> state -> unit
    (** This method is called when the [ftp_client_pi] is connected and
      * the queue of commands is empty.
     *)

end


exception FTP_method_failure of int * string
  (** This exception may be raised during execution by the FTP method.
    * The int is the unexpected FTP control code and the string the
    * corresponding text.
    *
    * {b This does not work yet as intended!}
   *)


class connect_method : host:string ->
                       ?port:int ->
                       unit -> ftp_method
  (** This method connects to the [host] which must be an IP address *)


class login_method : user:string ->
                     get_password:(unit -> string) ->
                     get_account:(unit -> string) ->
                     unit ->
                       ftp_method
(** This FTP method logs the [user] in. [get_password] is called when
  * the FTP server asks for the password (may be skipped). [get_account]
  * is called when the server asks for the account ID (may be skipped).
 *)


class get_method : file:string ->
                   representation:representation ->
                   store:(state -> Ftp_data_endpoint.local_receiver) ->
                   unit ->
                     ftp_method
(** This FTP method walks to the right directory and gets [file] from
  * the server. The file is stored in the [local_receiver] that can be
  * obtained by calling the [store] function. The selected [representation]
  * remains unchanged.
  *
  * [file] should be a slash-separated path. It is always interpreted
  * relative to the home directory of the user (i.e. the active directory
  * after login).
 *)


(** The ftp client is a user session that may even span several connections.
  * However, only one server is connected at once.
 *)
class ftp_client :
        ?event_system:Unixqueue.event_system ->
        ?onempty:(unit -> unit) ->
        unit ->
object

  method add : ?onsuccess:(unit -> unit) ->
               ?onerror:(exn -> unit) ->
               ftp_method -> unit
    (** Adds an FTP method to the queue of methods to execute. It is no
      * problem to add the same method twice.
      *
      * When the method could be executed successfully, the function
      * [onsuccess] is called. If an error occurs, the function
      * [onerror] is called instead. (Only soft errors are reported
      * this way; the queue is continued to be executed.)
      *
      * {b TODO: [onerror] is currently not called!}
     *)
  
  method run : unit -> unit
    (** Starts the event system; same as [Unixqueue.run] *)

end


(** {2 Examples and Discussion}
  *
  * To download a single flat file from a server:
  *
  * {[ 
  *   let buffer = Buffer.create 1000 in
  *   let ch = new Netchannels.output_buffer buffer in
  *   let client = new ftp_client() in
  *   client # add (new connect_method ~host:"127.0.0.1");
  *   client # add (new login_method ~user:"foo"
  *                                  ~get_password:(fun () -> "password")
  *                                  ~get_account:(fun () -> "foo") ());
  *   client # add (new get_method ~file:"path/to/file"
  *                                ~representation:`Image
  *                                ~store:(fun _ -> `File_structure ch) ());
  *   client # run()
  * ]}
  *
  * The file is stored in [buffer]. By using a different netchannel, it
  * can be stored whereever wanted.
  *
  * This piece of code has the disadvantage that the client does not stop
  * when a method fails ({b in the current version, it always stops, but
  * in a disadvantegous manner - this will be changed}). Because of this,
  * it is better to execute the next method only when the previous
  * was successful:
  *
  * {[ 
  *   ...
  *   client # add ~onsuccess:(fun () ->
  *                               client # add ~onsuccess:...
                                               (new login_method 
                                                   ~user:"foo"
  *                                                ~get_password:(fun () -> "password")
  *                                                ~get_account:(fun () -> "foo") ());
  *                           )
  *                (new connect_method ~host:"127.0.0.1")
  * ]}
  *
  * Alternatively, one can also force that the execution stops by raising
  * an exception in the [onerror] callback ({b not implemented in the current
  * version}).
  *
  * To download a record-structured text file, use a [store] like:
  *
  * {[ 
  *    let ch = (as above) in
  *    let rec_ch = new Ftp_data_endpoint.write_out_record_channel
  *                       ~repr:(`ASCII_unix `Enc_iso88591)
  *                       ch
  *    ...
  *    ... ~store:(fun _ -> `Record_structure rec_ch)
  * ]}
  *
  * Here, the end-of-record is transcoded to newline. Note that the ASCII
  * variant ([`Enc_iso88591]) is ignored by [write_out_record_channel].
  * {b Open: How to select record structure using an FTP method.}
  *
  * Character conversions: To convert an EBCDIC file to ASCII, use
  * something like
  *
  * {[ 
  *    let ch = (as above) in
  *    let converter = new Ftp_data_endpoint.data_converter
  *                         ~fromrepr:(`EBCDIC `Enc_cp1047)
  *                         ~torepr:(`ASCII_unix `Enc_iso88591) in
  *    let ch_ebcdic = new Netchannels.output_filter converter ch in
  *    ...
  *    ... ~representation:(`EBCDIC None)
  *    ... ~store:(fun _ -> `File_structure ch_ebcdic)
  * ]}
  *
  * The class [data_converter] also performs the transformation of the 
  * end-of-line convention, unlike the similar class
  * [Netconversion.conversion_pipe].
 *)
