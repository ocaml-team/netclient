(* $Id: http_client_mt.ml 141 2005-07-26 13:31:31Z gerd $ *)

Http_client.init_mt
  ~create_lock_unlock_pair:
    (fun () ->
       let mutex = Mutex.create() in
       let lock() = Mutex.lock mutex in
       let unlock() = Mutex.unlock mutex in
       lock, unlock
    )
;;
